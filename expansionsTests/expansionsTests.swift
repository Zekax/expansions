//
//  expansionsTests.swift
//  expansionsTests
//
//  Created by Jose Rodrigues on 04/08/2020.
//  Copyright © 2020 jose. All rights reserved.
//

import XCTest
import RestKit

@testable import expansions

class expansionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        let testTargetBundle = Bundle(identifier: "com.jose.expansionsTests")
        RKTestFixture.setFixtureBundle(testTargetBundle)
    }

    override func setUpWithError() throws {
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExpansionMapping() {
        let parsedJson = RKTestFixture.parsedObject(withContentsOfFixture: "expansions.json")
        let test = RKMappingTest(for: ExpansionsManager.expansionMapping(), sourceObject: parsedJson, destinationObject: nil)
        test?.addExpectation(RKPropertyMappingTestExpectation(sourceKeyPath: "id", destinationKeyPath: "id", value:nil))
        
        XCTAssert(test!.evaluate)
    }
    
    func testExpansionCardMapping() {
        let parsedJson = RKTestFixture.parsedObject(withContentsOfFixture: "expansionCard.json")
        let test = RKMappingTest(for: ExpansionsManager.expansionCardMapping(), sourceObject: parsedJson, destinationObject: nil)
        test?.addExpectation(RKPropertyMappingTestExpectation(sourceKeyPath: "imageUrls", destinationKeyPath: "imageUrls", value:nil))
        
        XCTAssert(test!.evaluate)
    }
    
    func testRequestExpansions(){
        let promise = expectation(description: "GET success")
        var statusCode: Int?
        var responseError: Error?
        ExpansionsManager.requestExpansions(callback: {(error, result, code) in
            responseError = error
            statusCode = code
            promise.fulfill()
        })
        wait(for: [promise], timeout: 25)
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }
       
    func testRequestExpansionCard(){
        let promise = expectation(description: "GET success")
        var statusCode: Int?
        var responseError: Error?
        ExpansionsManager.requestExpansionCards(for: "DoubleMasters", callback: {(error, result, code) in
            responseError = error
            statusCode = code
            promise.fulfill()
        })
        wait(for: [promise], timeout: 25)
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }


    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

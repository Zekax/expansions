//
//  ExpansionsManager.swift
//  expansions
//
//  Created by Jose Rodrigues on 05/08/2020.
//  Copyright © 2020 jose. All rights reserved.
//

import Foundation
import RestKit

class ExpansionsManager: NSObject{
    
    static func expansionMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: Expansion.self)
        mapping?.addAttributeMappings(from: ["id", "name", "code", "releaseDate", "cardCount"])
//        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "allExpansionCards", toKeyPath: "allExpansionCards", with: expansionCardMapping()))
        return mapping!
    }
    
    static func expansionCardMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: ExpansionCard.self)
        mapping?.addAttributeMappings(from: ["providerPrices", "priceId", "expansionName", "expansionFriendlyId", "artist", "rarity", "rarityName", "number", "friendlyId"])
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "name", toKeyPath: "name", with: localizableMapping()))
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "type", toKeyPath: "type", with: localizableMapping()))
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "imageUrls", toKeyPath: "imageUrls", with: imageUrlMapping()))
        return mapping!
    }
    
    static func localizableMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: Localizable.self)
        mapping?.addAttributeMappings(from: ["en", "cn", "tw", "fr", "de", "it", "jp", "pt", "ru", "es"])
        return mapping!
    }
    
    static func imageUrlMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: ImageUrl.self)
        mapping?.addAttributeMappings(from: ["small", "normal", "large", "png", "artcrop"])
        return mapping!
    }
    
    //MARK: - Requests
    static func requestExpansions(callback: @escaping rkCallbackFunc){
        let path = urlPath.expansions.rawValue
        let resDescriptor = RKResponseDescriptor(mapping: expansionMapping(), method: RKRequestMethod.GET, pathPattern:nil, keyPath: "data", statusCodes: RKStatusCodeIndexSetForClass(RKStatusCodeClass.successful))
        ServiceManager.requestOperation(url: path, responseDescriptor: resDescriptor!, requestComplete: callback)
    }
    
    static func requestExpansionCards(for expansionId: String, callback: @escaping rkCallbackFunc){
        OperationQueue().cancelAllOperations()
        let path = String(format: urlPath.cards.rawValue, expansionId)
        let resDescriptor = RKResponseDescriptor(mapping: expansionCardMapping(), method: RKRequestMethod.GET, pathPattern:nil, keyPath: "data.allExpansionCards", statusCodes: RKStatusCodeIndexSetForClass(RKStatusCodeClass.successful))
        ServiceManager.requestOperation(url: path, responseDescriptor: resDescriptor!, requestComplete: callback)
    }
}

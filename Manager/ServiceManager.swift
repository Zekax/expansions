//
//  ServiceManager.swift
//  expansions
//
//  Created by Jose Rodrigues on 05/08/2020.
//  Copyright © 2020 jose. All rights reserved.
//

import Foundation
import RestKit

typealias rkCallbackFunc = (NSError?, Any?, Int?) -> ()

enum urlPath:String{
    case expansions = "https://cdn.bigar.com/mtg/cardjson/expansions"
    case cards = "https://cdn.bigar.com/mtg/cardjson/expansions/%@"
}

class ServiceManager: NSObject{
    
    // MARK: - REQUEST
            
    /// Unmanaged object request, use when Core Data persistence is not needed.
    ///
    /// - Parameters:
    ///   - url: the url to make the request
    ///   - responseDescriptor: the response descriptor containing the object mapping and request method
    ///   - requestComplete: callback function to execute on request response
    static func requestOperation(url: String, responseDescriptor: RKResponseDescriptor, headerValues: [String:String]? = nil, requestComplete:rkCallbackFunc? = nil){
        
        var request = URLRequest(url: URL(string:url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)! )
        
        if headerValues != nil{
            for hv in headerValues!{
                request.setValue(hv.value, forHTTPHeaderField: hv.key)
            }
        }
        
        let operation = RKObjectRequestOperation(request: request, responseDescriptors: [responseDescriptor])
    
        operation?.setCompletionBlockWithSuccess({(moperation, mappingResult) -> Void in
            var result:Any?
            if mappingResult?.count ?? 0 > UInt(1){
                result = mappingResult?.array()
            }else{
                result = mappingResult?.firstObject
            }
            
            requestComplete?(nil, result, moperation?.httpRequestOperation.response.statusCode)
            moperation?.cancel()
        }, failure: { (moperation, error) -> Void in
            guard let nsError = error as NSError? else{
                return
            }
            requestComplete?(nsError, nil, moperation?.httpRequestOperation.response?.statusCode)
            moperation?.cancel()
        })
        let operationQueue = OperationQueue()
        operationQueue.addOperation(operation!)
    }
}



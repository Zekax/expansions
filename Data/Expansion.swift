//
//  Expansion.swift
//  expansions
//
//  Created by Jose Rodrigues on 05/08/2020.
//  Copyright © 2020 jose. All rights reserved.
//

import Foundation

class Expansion: NSObject{
    @objc dynamic var id                : String?
    @objc dynamic var name              : String?
    @objc dynamic var code              : String?
    @objc dynamic var releaseDate       : String?
    @objc dynamic var cardCount         : Int = 0
}

class ExpansionCard: NSObject{
    @objc dynamic var imageUrls             : NSSet?
    @objc dynamic var providerPrices        : NSSet?
    @objc dynamic var priceId               : String?
    @objc dynamic var expansionName         : String?
    @objc dynamic var expansionFriendlyId   : String?
    @objc dynamic var artist                : String?
    @objc dynamic var rarity                : String?
    @objc dynamic var rarityName            : String?
    @objc dynamic var name                  : Localizable?
    @objc dynamic var type                  : Localizable?
    @objc dynamic var number                : Int = 0
    @objc dynamic var friendlyId            : String?
}

class Localizable: NSObject{
    @objc dynamic var en: String?
    @objc dynamic var cn: String?
    @objc dynamic var tw: String?
    @objc dynamic var fr: String?
    @objc dynamic var de: String?
    @objc dynamic var it: String?
    @objc dynamic var jp: String?
    @objc dynamic var pt: String?
    @objc dynamic var ru: String?
    @objc dynamic var es: String?
    
    func systemLocal() -> String{
        
        switch Locale.preferredLanguages[0].prefix(2){
            case "en": return en ?? ""
            case "cn": return cn ?? ""
            case "tw": return tw ?? ""
            case "fr": return fr ?? ""
            case "de": return de ?? ""
            case "it": return it ?? ""
            case "jp": return jp ?? ""
            case "pt": return pt ?? ""
            case "ru": return ru ?? ""
            case "es": return es ?? ""
            
        default:
            break
        }
        return ""
    }
}

class ImageUrl: NSObject{
    @objc dynamic var small: String?
    @objc dynamic var normal: String?
    @objc dynamic var large: String?
    @objc dynamic var png: String?
    @objc dynamic var artcrop: String?
}

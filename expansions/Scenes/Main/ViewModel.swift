//
//  ViewModel.swift
//  expansions
//
//  Created by Jose Rodrigues on 06/08/2020.
//  Copyright © 2020 jose. All rights reserved.
//

import Foundation

protocol ExpansionsRequestProtocol:class{
    func responseExpansions(_ success:Bool)
    func responseCard(_ success:Bool)
}

class ViewModel: NSObject{
    weak var delegate: ExpansionsRequestProtocol?
    
    var expansionsList  = [Expansion]()
    var cardsList       = [ExpansionCard]()
    
    func requestExpansions(){
        ExpansionsManager.requestExpansions(callback: {(error, result, code) in
            guard error == nil else{
                self.delegate?.responseExpansions(false)
                return
            }
            
            self.expansionsList.removeAll()
            
            guard let list = result as? [Expansion] else{
                guard let item = result as? Expansion else{
                   self.delegate?.responseExpansions(true)
                   return
                }

                self.expansionsList.append(item)
                self.delegate?.responseExpansions(true)
                return
            }
            
            self.expansionsList = list
            self.delegate?.responseExpansions(true)
        })
    }
    
    func requestExpansionCards(for expansionId: String){
        ExpansionsManager.requestExpansionCards(for: expansionId, callback: {(error, result, code) in
            guard error == nil else{
                self.delegate?.responseCard(false)
                return
            }
            
            self.cardsList.removeAll()
            
            guard let list = result as? [ExpansionCard] else{
                guard let item = result as? ExpansionCard else{
                   self.delegate?.responseCard(true)
                   return
                }

                self.cardsList.append(item)
                self.delegate?.responseCard(true)
                return
            }
            
            self.cardsList = list
            self.delegate?.responseCard(true)
        })
    }
    
    //MARK: - Expansions
    func getExpansionsCount() -> Int{
        self.expansionsList.count
    }
    
    func getExpansionName(for index: Int) -> String{
        self.expansionsList[index].name ?? ""
    }
    
    func getExpansionID(for index: Int) -> String{
        self.expansionsList[index].id ?? ""
    }
    
    func getExpansionDate(for index: Int) -> String{
        self.expansionsList[index].releaseDate ?? ""
    }
    
    func getExpansionCode(for index: Int) -> String{
        self.expansionsList[index].code ?? ""
    }
    
    func getExpansionCardCount(for index: Int) -> String{
        "#" + self.expansionsList[index].cardCount.description
    }
    
    
    //MARK: - Cards
    
    func getCardImage(for index: Int) -> String{
        let imageSet = self.cardsList[index].imageUrls as! Set<ImageUrl>
        let image = imageSet.first
        return image?.small ?? ""
    }
    
    func getCardArtist(for index: Int) -> String{
        self.cardsList[index].artist ?? ""
    }
    
    func getCardRarity(for index: Int) -> String{
        self.cardsList[index].rarityName ?? ""
    }
    
    func getCardName(for index: Int) -> String{
        self.cardsList[index].name?.systemLocal() ?? ""
    }
    
    func getCardType(for index: Int) -> String{
        self.cardsList[index].type?.systemLocal() ?? ""
    }
    
    func getCardsCount() -> Int{
        self.cardsList.count
    }
    
}

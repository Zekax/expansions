//
//  InfoView.swift
//  expansions
//
//  Created by Jose Rodrigues on 06/08/2020.
//  Copyright © 2020 jose. All rights reserved.
//

import UIKit

class InfoView: UIView {
    
    init(){
        super.init(frame: CGRect.zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica-Bold", size: 32)
        label.textColor = .darkGray
        return label
    }()
    
    var dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 18)
        label.textColor = .lightGray
        return label
    }()
    

    var codeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 18)
        label.textColor = .lightGray
        return label
    }()
    
    var cardCountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 18)
        label.textColor = .lightGray
        return label
    }()

    private func setupUI() {
        self.backgroundColor = .white
        
        self.addSubview(nameLabel)
        self.addSubview(dateLabel)
        self.addSubview(codeLabel)
        self.addSubview(cardCountLabel)
        
        nameLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(self).offset(10)
            make.top.equalToSuperview().offset(10)
        }
        
        dateLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(self).offset(10)
            make.top.equalTo(nameLabel.snp.bottom).offset(10)
        }
        
        codeLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(self).offset(10)
            make.top.equalTo(dateLabel.snp.bottom).offset(10)
        }
        
        cardCountLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(self).offset(10)
            make.top.equalTo(codeLabel.snp.bottom).offset(10)
        }
    }
}

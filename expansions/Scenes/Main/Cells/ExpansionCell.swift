//
//  ExpansionCell.swift
//  expansions
//
//  Created by Jose Rodrigues on 06/08/2020.
//  Copyright © 2020 jose. All rights reserved.
//

import UIKit

class ExpansionCell: UICollectionViewCell {
    
    var view: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.backgroundColor = .lightText
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.darkGray.cgColor
        return view
    }()
    
    var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica-Bold", size: 32)
        label.numberOfLines = 0
        label.textColor = .darkGray
        return label
    }()
    
    var dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 18)
        label.textColor = .lightGray
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        contentView.addSubview(view)
        view.addSubview(nameLabel)
        view.addSubview(dateLabel)
        
        view.snp.makeConstraints { make in
            make.margins.equalToSuperview().offset(10)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view).offset(10)
            make.top.equalToSuperview().offset(10)
        }
        
        dateLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view).offset(10)
            make.top.equalTo(nameLabel.snp.bottom).offset(10)
        }
    }
}

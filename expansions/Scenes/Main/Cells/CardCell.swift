//
//  CardCell.swift
//  expansions
//
//  Created by Jose Rodrigues on 06/08/2020.
//  Copyright © 2020 jose. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {
    var imageUrl: String = String(){
        didSet{
            cardImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage())
        }
    }
    
    var cardImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica-Bold", size: 18)
        label.numberOfLines = 0
        label.textColor = .darkGray
        return label
    }()
    
    var artistLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 18)
        label.textColor = .lightGray
        return label
    }()
    
    var rarityLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 12)
        label.textColor = .green
        return label
    }()
    
    var typeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 12)
        label.textColor = .red
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
       super.init(style: style, reuseIdentifier: reuseIdentifier)
       self.setupUI()
    }

    required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.backgroundColor = .white
        contentView.backgroundColor = .white
        contentView.addSubview(cardImageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(artistLabel)
        contentView.addSubview(rarityLabel)
        contentView.addSubview(typeLabel)
        
        cardImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(5)
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().inset(10)
            make.width.equalTo(80)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.equalTo(cardImageView.snp.trailing).offset(5)
            make.top.equalToSuperview().offset(15)
        }
        
        rarityLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(5)
            make.bottom.equalTo(nameLabel.snp.bottom)
        }
        
        typeLabel.snp.makeConstraints { make in
            make.leading.equalTo(cardImageView.snp.trailing).offset(5)
            make.top.equalTo(nameLabel.snp.bottom).offset(5)
        }

        artistLabel.snp.makeConstraints { make in
            make.leading.equalTo(cardImageView.snp.trailing).offset(5)
            make.bottom.equalToSuperview().inset(15)
        }


        
    }
}

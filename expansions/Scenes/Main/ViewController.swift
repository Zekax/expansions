//
//  ViewController.swift
//  expansions
//
//  Created by Jose Rodrigues on 04/08/2020.
//  Copyright © 2020 jose. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

enum CellIdentifier: String{
    case collection = "collectionCellIdentifier"
    case table      = "tableCellIdentifier"
}

class ViewController: UIViewController {
    var page = 0
    private var indexOfCellBeforeDragging = 0
    
    let viewModel: ViewModel = {
        return ViewModel()
    }()
    
    private lazy var collectionView: UICollectionView = {
        
        let flow = UICollectionViewFlowLayout()
        let w = self.view.frame.size.width - 20
        let h = CGFloat(180.0)
        flow.itemSize = CGSize(width: w, height: h)
        flow.scrollDirection = .horizontal
        flow.minimumLineSpacing = 10
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flow)
        collectionView.delegate = self
        collectionView.dataSource = self
//        collectionView.isPagingEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.bounces = true
        collectionView.backgroundColor = .white
        collectionView.register(ExpansionCell.self, forCellWithReuseIdentifier: CellIdentifier.collection.rawValue)
        
        return collectionView
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = true
        tableView.bounces = true
        tableView.separatorStyle = .singleLine
        tableView.backgroundColor = .white
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = 100
        
        tableView.register(CardCell.self, forCellReuseIdentifier: "tableCellIdentifier")
        return tableView
    }()
    
    private lazy var infoView: InfoView = {
        let view = InfoView()
        view.backgroundColor = .white
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        self.view.backgroundColor = .white
        setup()
        
        viewModel.requestExpansions()
    }

    private func setup() {
        view.addSubview(collectionView)
        view.addSubview(infoView)
        view.addSubview(tableView)
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view)
            make.height.equalTo(200).priority(250)
            make.top.equalTo(view).offset(10)
        }
        
        infoView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view)
            make.height.equalTo(200).priority(250)
            make.top.equalTo(collectionView.snp.bottom).offset(10)
        }
        
        tableView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view)
            make.top.equalTo(infoView.snp.bottom)
            make.bottom.equalTo(view)
        }
        
        configureCollectionViewLayoutItemSize()
    }
    
    private func calculateSectionInset() -> CGFloat {
        let deviceIsIpad = UIDevice.current.userInterfaceIdiom == .pad
        let deviceOrientationIsLandscape = UIDevice.current.orientation.isLandscape
        let cellBodyViewIsExpended = deviceIsIpad || deviceOrientationIsLandscape
        let cellBodyWidth: CGFloat = 236 + (cellBodyViewIsExpended ? 174 : 0)
        
        let buttonWidth: CGFloat = 50
        
        let inset = (self.view.frame.size.width - cellBodyWidth + buttonWidth) / 4
        return inset
    }
    
    private func configureCollectionViewLayoutItemSize() {
        let collectionViewLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let inset = calculateSectionInset()
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        
        collectionViewLayout.itemSize = CGSize(width: self.view.frame.size.width - inset * 2, height: 180)
        collectionViewLayout.collectionView!.reloadData()
    }
    
    private func indexOfMajorCell() -> Int {
        let collectionViewLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout

        let itemWidth = collectionViewLayout.itemSize.width
        let proportionalOffset = collectionViewLayout.collectionView!.contentOffset.x / itemWidth
        let index = Int(round(proportionalOffset))
        let safeIndex = max(0, min(viewModel.getExpansionsCount() - 1, index))
        
        return safeIndex
    }
    
    private func fillData(for index: Int){
        infoView.nameLabel.text = viewModel.getExpansionName(for: index)
        infoView.dateLabel.text = viewModel.getExpansionDate(for: index)
        infoView.codeLabel.text = viewModel.getExpansionCode(for: index)
        infoView.cardCountLabel.text = viewModel.getExpansionCardCount(for: index)
        viewModel.requestExpansionCards(for: viewModel.getExpansionID(for: index))
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.getExpansionsCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.collection.rawValue, for: indexPath) as! ExpansionCell
        cell.nameLabel.text = viewModel.getExpansionName(for: indexPath.row)
        cell.dateLabel.text = viewModel.getExpansionDate(for: indexPath.row)
        return cell
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // Stop scrollView sliding:
        targetContentOffset.pointee = scrollView.contentOffset
        // Calculate where scrollView should snap to:
        let indexOfMajorCell = self.indexOfMajorCell()
        let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        if page != indexPath.row{
            fillData(for: indexPath.row)
        }
        page = indexPath.row
        print("page: \(indexPath.row)")
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
}

extension ViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.getCardsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCellIdentifier", for: indexPath) as! CardCell
        print(viewModel.getCardName(for: indexPath.row))
        cell.nameLabel.text = viewModel.getCardName(for: indexPath.row)
        cell.artistLabel.text = viewModel.getCardArtist(for: indexPath.row)
        cell.rarityLabel.text = viewModel.getCardRarity(for: indexPath.row)
        cell.typeLabel.text = viewModel.getCardType(for: indexPath.row)
        cell.imageUrl = viewModel.getCardImage(for: indexPath.row)
        return cell
    }
}

extension ViewController: ExpansionsRequestProtocol{
    func responseExpansions(_ success: Bool) {
        collectionView.reloadData()
        fillData(for: 0)
    }
    
    func responseCard(_ success: Bool) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
}
